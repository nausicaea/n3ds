use core::fmt;
use core::intrinsics;

/// Rust `core` requires this function for handling panics.
#[lang = "panic_fmt"]
#[no_mangle]
pub extern "C" fn rust_begin_panic(_msg: fmt::Arguments, _file: &'static str, _line: u32, _column: u32) -> ! {
    unsafe { intrinsics::abort() }
}
