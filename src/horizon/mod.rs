pub mod sync;
pub mod syscalls;
pub mod types;

pub struct CtrApplication {
    address_arbiter: sync::AddressArbiter,
}

impl CtrApplication {
    /// Initialize the Nintendo Horizon/CTR application.
    pub fn new() -> Result<Self, syscalls::error::Error> {
        // Clears the bss section
        unsafe { Self::clear_bss(); }

        // Saves the current stack pointer.

        // Calls __libctru_init(void (*retAddr)(void)) (a C function)
        // - Calls __sync_init() (a C function)
        //   - Calls svcCreateAddressArbiter(&arbiter) where arbiter: static Handle
        let arbiter = sync::AddressArbiter::new()?;
        // - Calls __system_initSyscalls() (a C function)
        //   - Registers newlib syscalls (exit = __ctru_exit, gettod_r = __libctru_gtod, getreent = __ctru_get_reent)
        //   - Registers locking syscalls
        //   - Initialises thread variables for the main thread
        //   - Copies data for thead local storage (TLS) with memcpy
        // - Calls __system_allocateHeaps() (a C function)
        //   - If the current heap size __ctru_heap_size is empty, allocates all remaining free memory
        //   - Calls svcControlMemory(...) to allocate the application heap
        //   - Calls svcControlMemory(...) to allocate the linear heap
        //   - Sets the newlib variables fake_heap_start and fake_heap_end
        // - Calls __system_initArgv() (a C function)
        //   - Initialises __system_argc and __system_argv

        // Adjust the stack and the heap somehow.
        // ldr r2, =global_fake_heap_start
        // ldr sp, [r2]
        // ldr r3, =global_stack_size
        // ldr r3, [r3]
        // add sp, sp, r3
        // add sp, sp, #7
        // bics sp, sp, #7
        // str sp, [r2]

        // Calls __appInit() (a C function)
        // - Calls srvInit() (a C function) for service management
        // - Calls aptInit() (a C function) for Applet/NS shell interaction
        // - Calls hidInit() (a C function) for Human Interface Device integration
        // - Calls fsInit() (a C function) for file system integration
        // - Calls sdmcInit() (a C function) for the SDMC driver
        // Calls __libc_init_array() (a C function) which iterates over libc-internal initialisation functions

        Ok(CtrApplication {
            address_arbiter: arbiter,
        })
    }

    /// Deinitialize the Nintendo Horizon/CTR application.
    pub fn destroy(&mut self) -> Result<(), syscalls::error::Error> {
        // Calls __libc_fini_array() (a C function) which iterates over libc-internal cleanup functions
        // Calls __appExit() (a C function)
        // - Calls sdmcExit()
        // - Calls fsExit()
        // - Calls hidExit()
        // - Calls aptExit()
        // - Calls srvExit()

        // Restores the saved stack pointer

        // Calls __libctru_exit(int rc) (a C function)
        // - Unmaps the linear heap
        // - Unmaps the application heap
        // - Calls envDestroyHandles() which destroys handles to services from the service manager
        // - Calls __sync_fini() if the pointer to that function is not null
        self.address_arbiter.destroy()?;
        // - Calls __system_retAddr if the pointer to that function is not null
        // - Calls svcExitProcess() otherwise

        Ok(())
    }

    /// Fills the `.bss` section with zeros.
    unsafe fn clear_bss() {
        asm!(r#"
            @ Calculate the length of the BSS section, based on values provided by the
            @ linker layout script.
            ldr r0, =__bss_start__
            ldr r1, =__bss_end__
            sub r1, r1, r0

            @ Clear the second significant bit of the BSS length value and exit the
            @ function, if the value is zero.
            mov r2, #3
            add r1, r1, r2
            bics r1, r1, r2
            beq end

            @ Otherwise, iterate over the BSS section (starting at the beginning),
            @ and zero its contents.
        loop:
            stmia r0!, {r2}
            subs r1, r1, #4
            bne loop
        end:
            nop
        "# : : : : "volatile");
    }
}
