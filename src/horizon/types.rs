#[derive(Copy, Clone, PartialEq, Eq)]
pub struct Handle(u32);

impl Handle {
    pub fn new(id: u32) -> Self {
        Handle(id)
    }

    pub fn id(&self) -> u32 {
        self.0
    }
}
