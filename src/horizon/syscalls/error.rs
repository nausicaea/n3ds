use core::fmt;

#[derive(Clone, Copy, PartialEq, Eq)]
pub struct Error(i32);

impl fmt::Debug for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error({:?}, {:?}, {:?}, {:?})", self.level(), self.summary(), self.module(), self.description())
    }
}

impl From<i32> for Error {
    fn from(value: i32) -> Self {
        Error(value)
    }
}

impl Error {
    pub fn is_success(&self) -> bool {
        !self.is_failure()
    }

    pub fn is_failure(&self) -> bool {
        self.0 < 0
    }

    pub fn level(&self) -> Level {
        From::from((self.0 >> 27) & 0x1f)
    }

    pub fn summary(&self) -> Summary {
        From::from((self.0 >> 21) & 0x3f)
    }

    pub fn module(&self) -> Module {
        From::from((self.0 >> 10) & 0xff)
    }

    pub fn description(&self) -> Description {
        From::from(self.0 & 0x3ff)
    }
}

#[derive(Debug)]
pub enum Level {
    Success,
    Info,
    Fatal,
    Reset,
    Reinitialize,
    Usage,
    Permanent,
    Temporary,
    Status,
    InvalidResultValue,
}

impl From<i32> for Level {
    fn from(value: i32) -> Level {
        match value {
            0x00 => Level::Success,
            0x01 => Level::Info,
            0x1f => Level::Fatal,
            0x1e => Level::Reset,
            0x1d => Level::Reinitialize,
            0x1c => Level::Usage,
            0x1b => Level::Permanent,
            0x1a => Level::Temporary,
            0x19 => Level::Status,
            _ => Level::InvalidResultValue,
        }
    }
}

#[derive(Debug)]
pub enum Summary {
    Success,
    Nop,
    WouldBlock,
    OutOfResource,
    NotFound,
    InvalidState,
    NotSupported,
    InvalidArgument,
    WrongArgument,
    Cancelled,
    StatusChanged,
    Internal,
    InvalidResultValue,
}

impl From<i32> for Summary {
    fn from(value: i32) -> Summary {
        match value {
            0x00 => Summary::Success,
            0x01 => Summary::Nop,
            0x02 => Summary::WouldBlock,
            0x03 => Summary::OutOfResource,
            0x04 => Summary::NotFound,
            0x05 => Summary::InvalidState,
            0x06 => Summary::NotSupported,
            0x07 => Summary::InvalidArgument,
            0x08 => Summary::WrongArgument,
            0x09 => Summary::Cancelled,
            0x0a => Summary::StatusChanged,
            0x0b => Summary::Internal,
            _ => Summary::InvalidResultValue,
        }
    }
}

#[derive(Debug)]
pub enum Module {
    Common,
    Kernel,
    Util,
    FileServer,
    LoaderServer,
    Tcb,
    Os,
    Dbg,
    Dmnt,
    Pdn,
    Gsp,
    I2c,
    Gpio,
    Dd,
    Codec,
    Spi,
    Pxi,
    Fs,
    Di,
    Hid,
    Cam,
    Pi,
    Pm,
    PmLow,
    Fsi,
    Srv,
    Ndm,
    Nwm,
    Soc,
    Ldr,
    Acc,
    Romfs,
    Am,
    Hio,
    Updater,
    Mic,
    Fnd,
    Mp,
    Mpwl,
    Ac,
    Http,
    Dsp,
    Snd,
    Dlp,
    HioLow,
    Csnd,
    Ssl,
    AmLow,
    Nex,
    Friends,
    Rdt,
    Applet,
    Nim,
    Ptm,
    Midi,
    Mc,
    Swc,
    Fatfs,
    Ngc,
    Card,
    Cardnor,
    Sdmc,
    Boss,
    Dbm,
    Config,
    Ps,
    Cec,
    Ir,
    Uds,
    Pl,
    Cup,
    Gyroscope,
    Mcu,
    Ns,
    News,
    Ro,
    Gd,
    CardSpi,
    Ec,
    WebBrowser,
    Test,
    Enc,
    Pia,
    Act,
    Vctl,
    Olv,
    Neia,
    Npns,
    Avd,
    L2b,
    Mvd,
    Nfc,
    Uart,
    Spm,
    Qtm,
    Nfp,
    Application,
    InvalidResultValue,
}

impl From<i32> for Module {
    fn from(value: i32) -> Module {
        match value {
            0x00 => Module::Common,
            0x01 => Module::Kernel,
            0x02 => Module::Util,
            0x03 => Module::FileServer,
            0x04 => Module::LoaderServer,
            0x05 => Module::Tcb,
            0x06 => Module::Os,
            0x07 => Module::Dbg,
            0x08 => Module::Dmnt,
            0x09 => Module::Pdn,
            0x0a => Module::Gsp,
            0x0b => Module::I2c,
            0x0c => Module::Gpio,
            0x0d => Module::Dd,
            0x0e => Module::Codec,
            0x0f => Module::Spi,
            0x10 => Module::Pxi,
            0x11 => Module::Fs,
            0x12 => Module::Di,
            0x13 => Module::Hid,
            0x14 => Module::Cam,
            0x15 => Module::Pi,
            0x16 => Module::Pm,
            0x17 => Module::PmLow,
            0x18 => Module::Fsi,
            0x19 => Module::Srv,
            0x1a => Module::Ndm,
            0x1b => Module::Nwm,
            0x1c => Module::Soc,
            0x1d => Module::Ldr,
            0x1e => Module::Acc,
            0x1f => Module::Romfs,
            0x20 => Module::Am,
            0x21 => Module::Hio,
            0x22 => Module::Updater,
            0x23 => Module::Mic,
            0x24 => Module::Fnd,
            0x25 => Module::Mp,
            0x26 => Module::Mpwl,
            0x27 => Module::Ac,
            0x28 => Module::Http,
            0x29 => Module::Dsp,
            0x2a => Module::Snd,
            0x2b => Module::Dlp,
            0x2c => Module::HioLow,
            0x2d => Module::Csnd,
            0x2e => Module::Ssl,
            0x2f => Module::AmLow,
            0x30 => Module::Nex,
            0x31 => Module::Friends,
            0x32 => Module::Rdt,
            0x33 => Module::Applet,
            0x34 => Module::Nim,
            0x35 => Module::Ptm,
            0x36 => Module::Midi,
            0x37 => Module::Mc,
            0x38 => Module::Swc,
            0x39 => Module::Fatfs,
            0x3a => Module::Ngc,
            0x3b => Module::Card,
            0x3c => Module::Cardnor,
            0x3d => Module::Sdmc,
            0x3e => Module::Boss,
            0x3f => Module::Dbm,
            0x40 => Module::Config,
            0x41 => Module::Ps,
            0x42 => Module::Cec,
            0x43 => Module::Ir,
            0x44 => Module::Uds,
            0x45 => Module::Pl,
            0x46 => Module::Cup,
            0x47 => Module::Gyroscope,
            0x48 => Module::Mcu,
            0x49 => Module::Ns,
            0x4a => Module::News,
            0x4b => Module::Ro,
            0x4c => Module::Gd,
            0x4d => Module::CardSpi,
            0x4e => Module::Ec,
            0x4f => Module::WebBrowser,
            0x50 => Module::Test,
            0x51 => Module::Enc,
            0x52 => Module::Pia,
            0x53 => Module::Act,
            0x54 => Module::Vctl,
            0x55 => Module::Olv,
            0x56 => Module::Neia,
            0x57 => Module::Npns,
            0x5a => Module::Avd,
            0x5b => Module::L2b,
            0x5c => Module::Mvd,
            0x5d => Module::Nfc,
            0x5e => Module::Uart,
            0x5f => Module::Spm,
            0x60 => Module::Qtm,
            0x61 => Module::Nfp,
            0xfe => Module::Application,
            _ => Module::InvalidResultValue,
        }
    }
}

#[derive(Debug)]
pub enum Description {
    Success,
    InvalidResultValue,
    Timeout,
    OutOfRange,
    AlreadyExists,
    CancelRequested,
    NotFound,
    AlreadyInitialized,
    NotInitialized,
    InvalidHandle,
    InvalidPointer,
    InvalidAddress,
    NotImplemented,
    OutOfMemory,
    MisalignedSize,
    MisalignedAddress,
    Busy,
    NoData,
    InvalidCombination,
    InvalidEnumValue,
    InvalidSize,
    AlreadyDone,
    NotAuthorized,
    TooLarge,
    InvalidSelection,
}

impl From<i32> for Description {
    fn from(value: i32) -> Description {
        match value {
            0x000 => Description::Success,
            0x3fe => Description::Timeout,
            0x3fd => Description::OutOfRange,
            0x3fc => Description::AlreadyExists,
            0x3fb => Description::CancelRequested,
            0x3fa => Description::NotFound,
            0x3f9 => Description::AlreadyInitialized,
            0x3f8 => Description::NotInitialized,
            0x3f7 => Description::InvalidHandle,
            0x3f6 => Description::InvalidPointer,
            0x3f5 => Description::InvalidAddress,
            0x3f4 => Description::NotImplemented,
            0x3f3 => Description::OutOfMemory,
            0x3f2 => Description::MisalignedSize,
            0x3f1 => Description::MisalignedAddress,
            0x3f0 => Description::Busy,
            0x3ef => Description::NoData,
            0x3ee => Description::InvalidCombination,
            0x3ed => Description::InvalidEnumValue,
            0x3ec => Description::InvalidSize,
            0x3eb => Description::AlreadyDone,
            0x3ea => Description::NotAuthorized,
            0x3e9 => Description::TooLarge,
            0x3e8 => Description::InvalidSelection,
            _ => Description::InvalidResultValue,
        }
    }
}
