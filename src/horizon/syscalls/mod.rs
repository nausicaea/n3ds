pub mod error;

use core::intrinsics;
use super::types;
use self::error::Error;

pub fn illegal_call(test_value: u32) -> ! {
    unsafe {
        asm!("svc 0x0" : : "{r7}"(test_value) : : "volatile");
        intrinsics::unreachable()
    }
}

pub fn exit_process() -> ! {
    unsafe {
        asm!("svc 0x3" : : : : "volatile");
        intrinsics::unreachable()
    }
}

pub fn create_address_arbiter() -> Result<types::Handle, Error> {
    let result_code: i32;
    let handle_id: u32;
    unsafe {
        asm!(r#"
            svc 0x21
        "# : "={r0}"(result_code), "={r1}"(handle_id) : : : "volatile");
    }

    let error: Error = result_code.into();

    if error.is_success() {
        Ok(types::Handle::new(handle_id))
    } else {
        Err(error)
    }
}

pub fn close_handle(handle: types::Handle) -> Result<(), Error> {
    let result_code: i32;

    unsafe {
        asm!(r#"
            svc 0x23
        "# : "={r0}"(result_code) : "{r0}"(handle.id()) : : "volatile");
    }

    let error: Error = result_code.into();

    if error.is_success() {
        Ok(())
    } else {
        Err(error)
    }
}
