use super::types;
use super::syscalls;

pub struct AddressArbiter {
    handle: types::Handle,
}

impl AddressArbiter {
    pub fn new() -> Result<Self, syscalls::error::Error> {
        Ok(AddressArbiter {
            handle: syscalls::create_address_arbiter()?,
        })
    }

    pub fn destroy(&mut self) -> Result<(), syscalls::error::Error> {
        syscalls::close_handle(self.handle)
    }
}
