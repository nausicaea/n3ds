use core::intrinsics;
use core::mem;
use core::ptr;
use horizon;

#[repr(C)]
pub struct Stack {
    argc: isize,
    argv0: &'static u8,
}

impl Stack {
    fn argc(&self) -> isize {
        self.argc
    }

    fn argv(&self) -> *const *const u8 {
        &self.argv0 as *const &'static u8 as *const *const u8
    }
}

#[link_section = ".crt0"]
#[export_name = "_start"]
#[naked]
pub extern "C" fn entry() -> ! {
    unsafe {
        asm!(r#"
            mov r0, sp
            mov r1, lr
            b _start_rust
        "# : : : : "volatile");
        intrinsics::unreachable()
    }
}

#[inline(never)]
#[export_name = "_start_rust"]
pub extern "C" fn start(_stack: &'static Stack, system_lr: *const u8) -> ! {
    extern "C" {
        fn main(argc: isize, argv: *const *const u8) -> isize;
    }

    // Initialize the system
    {
        let mut ctru = horizon::CtrApplication::new().unwrap();

        // Call the main function
        unsafe {
            main(0, ptr::null());
        }

        // Deinitialize the system
        ctru.destroy().unwrap();
    }

    // Exit
    if system_lr.is_null() {
        horizon::syscalls::exit_process()
    } else {
        unsafe {
            (mem::transmute::<_, fn() -> !>(system_lr))()
        }
    }
}

#[lang = "start"]
fn lang_start(main: *const u8, _argc: isize, _argv: *const *const u8) -> isize {
    unsafe {
        (mem::transmute::<_, fn()>(main))();
    }

    0
}
